# systraybrowser
systraybrowser is a system tray application with a window for displaying a single webpage or JavaScript 'webapp'.

## Compiling
systraybrowser is written in Vala, so make sure the vala compiler is installed on your system. Also, systraybrowser use Gtk3 and webkit. Since vala does not ship with a webkitgtk3 vapi file (needed for compiling), it will be necessary to move the included .deps and .vapi files to the vala vapi directory.

* as root, cp 'webkitgtk-3.0.deps'  and 'webkitgtk-3.0.vapi' to  /usr/share/vala-X.Y/vapi/

When you are ready to compile, run
`./build.sh`

## Using
  systraybrowser will try to load a webpage named index.html from ./www  
or /usr/local/share/systraybrowser/www   
or /usr/share/systraybrowser/www

### Getting Fancy
The path that is searched for index.html file is dependent upon the config.vala file's PACKAGE_NAME. By changing the PACKAGE_NAME before compiling, it is possible to search a different directory. 

For Example: Suppose a developer created a nifty javascript app named Yozer. By setting the PACKAGE_NAME to 'yozer', the compiled application will look for the index.html file in  
./www    
or /usr/local/share/yozer/www   
or /usr/share/yozer/www

### Tips for Devs
#### passing an icon from the rendered webpage to systraybrowser
systraybrowser doesn't have an icon. The icon is to be provided by the index.html's javascript as the text of an svg. To pass the svg text, in javascript run  
 `console.log("SVG_STRING::RAW-TEXT-OF-SVG-GOES-HERE");`

example:   
`console.log("SVG_STRING::<svg xmlns='http://www.w3.org/2000/svg' width='120' height='120'><path fill-opacity='0' stroke='#111' stroke-width='30' d='M 59.99083470583372 15.000000933362422 A 45 45 0 1 1 59.93749590278678 15.000043408489475 Z' </path></svg>");`

#### passing hover text for the tray icon
Similar to passing an icon, hover text can also be passed from the javascript to systraybrowser by calling  
`console.log("HOVER_TEXT::HOVER-TEXT-GOES-HERE");`

example:   
`console.log("HOVER_TEXT::Hey buddy! How's it going?")`