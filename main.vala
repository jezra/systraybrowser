/* Copyright 2016 Jezra Johnson Lickter */
/* Licensed GPLv3 */

using Gtk;
using WebKit;
using Gdk;


public class AppWindow : Gtk.Window {

	//what signals will this class emit?
	public signal void web_request(string uri);

	//what variable need to be persistant in scope?
	WebView web_view;
	StatusIcon si;
	Gtk.Menu iconMenu;
  string app_name;
  string app_version;
  bool iconified;
  PixbufLoader ploader;
	Pixbuf pb;


	public AppWindow(string app_name, string app_version) {
    this.app_name = app_name;
    this.app_version = app_version;
    //what happens when the Window Manager close button is clicked?
    delete_event.connect( cb_delete_event );
    //track the window state event
    window_state_event.connect( cb_window_state_event );
		//build the UI
		var scrolled_window = new ScrolledWindow(null,null);
		web_view = new WebView();
		//track the webview title changes
    web_view.title_changed.connect( cb_title_changed );
		//keep track of console messages
		web_view.console_message.connect( cb_console_message );

    //make a settings thingy for the webview we are going to create
		WebSettings settings = new WebSettings();
		/* set some properties of the settings thingy */
		//local storage
		settings.enable_html5_local_storage = true;
    // access remote content
    settings.enable_universal_access_from_file_uris = true;
    //apply the settings
    web_view.set_settings( settings );

		//show scrollbars if they are needed
		scrolled_window.set_policy(Gtk.PolicyType.AUTOMATIC,Gtk.PolicyType.AUTOMATIC);
		scrolled_window.add(web_view);
		add(scrolled_window);

		//resize(600,700);
		maximize();

		//create a statusicon
		si= new StatusIcon();
		si.set_tooltip_text("Hello World");
		si.activate.connect(toggle_visible);
		//we need to keep track of clicks on the status icon
		 si.popup_menu.connect(status_icon_popup);
		//create the icons menu
		iconMenu = new Gtk.Menu();
		//connect the signals of the icon menu
		var menuAbout = new ImageMenuItem.from_stock(Stock.ABOUT, null);
		menuAbout.activate.connect(display_about_dialog);
		iconMenu.append(menuAbout);
		var menuQuit = new ImageMenuItem.from_stock(Stock.QUIT, null);
		menuQuit.activate.connect(Gtk.main_quit);
		iconMenu.append(menuQuit);
		iconMenu.show_all();



		/* set up CTRL+r as refresh */
		//create an acccelerator group
		AccelGroup accel = new AccelGroup();
		//create the accelerator
		accel.connect(Gdk.keyval_from_name("r"),
		Gdk.ModifierType.CONTROL_MASK,
		Gtk.AccelFlags.VISIBLE,
		reload_page );
		//add the accelgroup to the window
		add_accel_group(accel);

		/* set up CTRL+q as quit */
		//create the accelerator
		accel.connect(Gdk.keyval_from_name("q"),
		Gdk.ModifierType.CONTROL_MASK,
		Gtk.AccelFlags.VISIBLE,
		quit );
		//add the accelgroup to the window
		add_accel_group(accel);
	}


	public bool reload_page() {
		print("reload\n");
		web_view.reload();
		return true;
	}

  public void run() {
    //load the www/index.html
     string[] possible_www_root = {
      /*where *could* the config be?*/
      //local directory
      GLib.Path.build_filename( GLib.Environment.get_current_dir(), "www", "index.html" ),
			//
      GLib.Path.build_filename("/usr","share", this.app_name, "www", "index.html"),
      GLib.Path.build_filename("/usr","local","share", this.app_name, "www", "index.html"),
    };

		foreach(string index in possible_www_root ) {
      //does the file exist?
      print (@"$index\n");
      if (FileUtils.test(index, FileTest.EXISTS) ) {

				//load the index in the webview
        web_view.load_uri("file://"+index);
        break;
      }
    }
    //show all elements
    show_all();
    iconify();
  }

  public void toggle_visible() {
    if (!iconified) {
      hide_window();
    } else {
      show_window();
    }
  }

	public bool cb_window_state_event(Gdk.EventWindowState state) {
		//holy fuck, bit shifting makes very little sense to me
		iconified = ( (state.new_window_state & Gdk.WindowState.ICONIFIED) == Gdk.WindowState.ICONIFIED);
		return true;
	}

	public void cb_title_changed() {
		set_title(web_view.get_title());
	}

  public bool cb_delete_event() {
    hide_window();
    return true;
  }

  private bool cb_console_message(string message, int line_number, string source_id) {
		//is the message an svg string?
		if (message.contains("SVG_STRING::")) {
			var bits = message.split("::");
			reset_icon(bits[1]);
		} else if (message.contains("HOVER_TEXT::")) {
			var bits = message.split("::");
			set_hover_text(bits[1]);
		}
		return false;
	}

	private void reset_icon(string svg_string) {
		print(@"$svg_string\n");
		//convert the str to uchar
		uchar[] svg_data = new uchar[svg_string.length];
		for (int i=0; i< svg_string.length; i++) {
			svg_data[i] = (uchar)svg_string[i];
		}
		// create a pixbufloader for svg
		ploader = new PixbufLoader.with_type("svg");
		//write the svg date to the loader
		ploader.write(svg_data);
		//close the loader
		ploader.close();
		//get a pixbuf from the loader
		pb = ploader.get_pixbuf();
		//set the tray icon to the pixbuf
		this.si.set_from_pixbuf(pb);
		//set the window icon to the pixbuf
		set_icon(pb);
	}

	private void set_hover_text(string hover_text) {
		si.set_tooltip_text(hover_text);
	}

  private void hide_window() {
    iconify();
  }

  private void show_window() {
    deiconify();
  }

	private bool quit(){
		Gtk.main_quit();
		return false;
	}
	/* Show popup menu on right button */
	private void status_icon_popup(uint button, uint time) {
		iconMenu.popup(null, null, null, button, time);
	}

	private void display_about_dialog(){
		//create an about dialog
		AboutDialog aboutDialog = new AboutDialog();
		//set the icon of the about window based on the pixbuf of the statusicon
		var pb = si.get_pixbuf();
		aboutDialog.set_icon(pb);
		aboutDialog.set_logo(pb);
		aboutDialog.set_program_name(this.app_name);
		aboutDialog.set_version(this.app_version);
		aboutDialog.set_comments("A data usage meter for HughesNet ISP");
		aboutDialog.set_copyright("© 2016 JezraCorp");
		aboutDialog.set_authors({"Jezra Johnson Lickter"});
    aboutDialog.set_license(null);
    aboutDialog.set_license_type(Gtk.License.GPL_3_0);
		//aboutDialog.show_all();
		aboutDialog.run();
		aboutDialog.destroy();
	}

}


//The main function, the App starts here.
private static void main (string[] args) {
  string name = Config.PACKAGE_NAME;
  string ver = Config.MAJOR_VERSION+"."+Config.MINOR_VERSION;
	//initialize Gtk
	Gtk.init (ref args);
	//fire up the controller, the controller will create the rest of the stuff
	var myapp = new AppWindow(name, ver);

  myapp.run();
  //run the mainloop
  Gtk.main();

}
